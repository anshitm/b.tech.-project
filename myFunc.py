import cv2
import csv
import numpy as np

def shapeMap(myTempImg,radIncr):
    tempImg = myTempImg.copy()
    hough_img = np.zeros_like(tempImg)
    circles = cv2.HoughCircles(tempImg,cv2.cv.CV_HOUGH_GRADIENT,2,30,param1=50,param2=20,minRadius=1,maxRadius=20)
    print "shapes count:", len(circles[0])

    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0,:]:
            cv2.circle(hough_img,(i[0],i[1]),i[2]+radIncr,255,-1)

    return hough_img

def normalizedImg(mImg):
    myImg = mImg.copy()
    myImg = myImg.astype(np.float)

    maxval = np.max(myImg)
    if(maxval!=0):
        myImg = myImg/maxval

    myImg = np.around(myImg)
    myImg = myImg.astype(np.uint8)
    return myImg


def drawMaps(featureName,featureLevels):
    i = 1
    for level in featureLevels:
        cv2.imshow(featureName+" : "+str(i),level)
        i = i+1

def build_filters():
    filters = []
    ksize = 31
    for theta in np.arange(0, np.pi, np.pi / 16):
        kern = cv2.getGaborKernel((ksize, ksize), 4.0, theta, 10.0, 0.5, 0, ktype=cv2.CV_32F)
        kern /= 1.5*kern.sum()
        filters.append(kern)
    return filters

def process(image, filters):
    img = image.copy()
    accum = np.zeros_like(img)
    for kern in filters:
        fimg = cv2.filter2D(img, cv2.CV_8UC3, kern)
        np.maximum(accum, fimg, accum)
    return accum

def featureMapsFromCenterSurround(image,area_offset,maxCandidates,ThresholdValue):
    TotalCandidates = 0
    temp_img = image.copy()
    img_levels = [temp_img]
    org_h,org_w = temp_img.shape
    feature_map = np.array([])
    feature_levels =  []

    for i in range(6):
        temp = cv2.pyrDown(img_levels[-1])
        img_levels.append(temp)

    while TotalCandidates<maxCandidates:
        ThresholdValue+=10
        TotalCandidates = 0
        binary_levels = []
        for c in range(1,4):
            for delta in range(2,4):
                h,w = img_levels[c].shape
                img_temp = cv2.resize(img_levels[c+delta],(w,h),fx=0,fy=0,interpolation = cv2.INTER_LINEAR)
                diff_temp = cv2.add(img_levels[c], -img_temp)

                diff_temp_f = diff_temp.astype(np.float)
                min_temp = np.amin(diff_temp_f)
                max_temp = np.amax(diff_temp_f)
                norm_temp = (diff_temp_f - min_temp)/(max_temp - min_temp)

                norm_zeroscale=cv2.resize(norm_temp,(org_w,org_h),fx=0,fy=0,interpolation = cv2.INTER_LINEAR)
                int_norm_zeroscale = np.uint8(norm_zeroscale*255)
                _,binary_temp = cv2.threshold(int_norm_zeroscale,ThresholdValue,255,cv2.THRESH_BINARY_INV) # the threshold value has to be iteratively changed

                i=5
                kernel_dilate = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(i,i))
                morphed= cv2.dilate(binary_temp,kernel_dilate,iterations = 1)
#                morphed = cv2.erode(dilated,kernel_erode,iterations = 1)

                contours, hierarchy = cv2.findContours(morphed,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
                good_contour_flags = np.array([cv2.contourArea(x)<area_offset for x in contours])
                org_contours = np.asarray(contours)
                good_contours = org_contours[good_contour_flags]
                good_contours = good_contours.tolist()

                hullcontours = []
                for cnt in good_contours:
                    hull = cv2.convexHull(cnt)
                    hullcontours.append(hull)

##                if len(good_contours)==0:
##                    print "hulls 0"
##                    cv2.waitKey(0)
                temp = np.zeros((org_h,org_w),np.uint8)
                cv2.drawContours(temp, hullcontours, -1,255, -1)

                myTemp = normalizedImg(temp)
                binary_levels.append(myTemp)

        binary_map = np.zeros((org_h,org_w),np.uint8);

        for level in binary_levels:
            binary_map = cv2.bitwise_or(binary_map,level)

        feature_map = binary_map.copy()
        feature_levels = binary_levels
        bMap = binary_map.copy()
        mapContours,_ = cv2.findContours(bMap,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        TotalCandidates = len(mapContours)
        print "thresh:",ThresholdValue," count:", TotalCandidates

    return feature_map, feature_levels, mapContours

def DataToCSV(myImg,mystr,training,labelledImg,area_offset,maxCandidates,thresholdVal):

    temp_img = myImg.copy()
    houghImg = shapeMap(temp_img,5)
#    cv2.imshow('houghImg',houghImg)
#    cv2.waitKey(0)

    shape_map = normalizedImg(houghImg)
    gabor_filters = build_filters()
    texture_img = process(myImg, gabor_filters)

    color_map, color_levels, color_points = featureMapsFromCenterSurround(myImg,area_offset,maxCandidates,thresholdVal)
    texture_map, texture_levels, texture_points = featureMapsFromCenterSurround(texture_img,area_offset,maxCandidates,thresholdVal)

    f = open('F:\\study8\\btp\\dataset\\my img\\csvfiles\\'+mystr+'_feature.csv','wb')
    csvWriter  = csv.writer(f)
    headings = ['x','y','colorMap_1','colorMap_2','colorMap_3','colorMap_4','colorMap_5','colorMap_6','textureMap_1','textureMap_2','textureMap_3','textureMap_4','textureMap_5','textureMap_6','shapeMap','trueValue']
    if(training): csvWriter.writerow(headings)
    else: csvWriter.writerow(headings[:-1])

    tempH,tempW = myImg.shape

    for i in range(tempH):
        for j in range(tempW):
            mylist = [i];
            mylist.append(j);

            for c_index in range(6):
                mylist.append(color_levels[c_index][i,j])
#                mylist.append(0)
            for t_index in range(6):
                mylist.append(texture_levels[t_index][i,j])
#                mylist.append(0)

            mylist.append(shape_map[i,j])
            if(training): mylist.append(labelledImg[i,j])

            csvWriter.writerow(mylist)
    f.close()

def outputImgFromCSV(testImg):
    tempH,tempW = testImg.shape
    resImg = np.zeros_like(testImg)

    myf = open('F:\\study8\\btp\\dataset\\my img\\csvfiles\\prediction.csv')
    csvReader  = csv.reader(myf)
    next(csvReader)

    count = 0
    for row in csvReader:
        i = count/tempW
        j = count%tempW
        resImg[i,j] = int(row[0])*255
        count = count + 1

    myf.close()
    return resImg
