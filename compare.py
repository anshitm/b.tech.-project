import numpy as np
import cv2

img1 = cv2.imread('F:\\study8\\btp\\dataset\\my img\\outputImg\\O26.jpg',0)
img2 = cv2.imread('F:\\study8\\btp\\dataset\\my img\\labelledImg\\L2.jpg',0)

res = cv2.bitwise_and(img1,img2)

cv2.imshow('res',res)
cv2.imshow('labelled',img2)
cv2.imshow('predicted',img1)

cv2.waitKey(0)
cv2.destroyAllWindows()
