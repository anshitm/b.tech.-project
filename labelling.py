import numpy as np
import cv2
from common import draw_str, RectSelector


def onrect(rect):
        x0, y0, x1, y1 = rect
        midx = (x0+x1)/2
        midy = (y0+y1)/2
        major_axis = abs(x0-x1)/2
        minor_axis = abs(y0-y1)/2
        cv2.ellipse(rect_img,(midx,midy),(major_axis,minor_axis),0,0,360,(0, 0, 255), -1)
        print rect

myImg  = cv2.imread('F:\\study8\\btp\\dataset\\my img\\trainedImg\\T3.jpg')
rect_img = np.zeros_like(myImg)
##img = np.zeros((300,300,3),np.uint8)

cv2.namedWindow('frame',cv2.WINDOW_AUTOSIZE)
rect_sel = RectSelector('frame', onrect)

while(1):
    img = myImg.copy()
    rect_sel.draw(img)
    img1 = cv2.add(img,rect_img)
    cv2.imshow('frame',img1)
    cv2.imshow('out',rect_img)
    if cv2.waitKey(10)==27:
        break

rect_img = cv2.cvtColor(rect_img,cv2.COLOR_BGR2GRAY)
_,binary_img = cv2.threshold(rect_img,50,255,cv2.THRESH_BINARY)

cv2.imwrite('F:\\study8\\btp\\dataset\\my img\\labelledImg\\L3.jpg',binary_img) #change the output file name
cv2.destroyAllWindows()